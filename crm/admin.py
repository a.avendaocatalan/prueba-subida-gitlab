from django.contrib import admin
from crm.models import Client, GroupClient, ProfitCenter, Area,TypeService,Service,ClientService
""" ● Todos los modelos deben tener sus campos listados a través de list_display
● En las entidades de cliente, servicio debe agregar al menos un filtro y una caja de
búsqueda
"""
@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
	list_display = (
		'name',
		'business_name',
		'address_1',
		'identification_type',
		'identification_number',
		'create_at',
		'modified_at',
		'tecnical_contact',
		'comercial_contact',
		'finance_contact',
		'group_client'
		)
	list_filter = ('name', 'business_name','address_1','identification_type','identification_number','tecnical_contact','comercial_contact','finance_contact','group_client','create_at','modified_at')
	search_fields = ('name', 'business_name','address_1','identification_type','identification_number','tecnical_contact','comercial_contact','finance_contact','group_client__name','create_at','modified_at')
	pass
@admin.register(GroupClient)
class GroupClientAdmin(admin.ModelAdmin):
	list_display = ('name', 'create_at','modified_at')
	list_filter = ('name', 'create_at')
	pass
@admin.register(Area)
class AreaAdmin(admin.ModelAdmin):
	list_display = ('name', 'create_at','update_at')
	list_filter = ('name', 'create_at')
	pass
@admin.register(ProfitCenter)
class ProfitCenterAdmin(admin.ModelAdmin):
	list_display = ('name', 'create_at','update_at')
	list_filter = ('name', 'create_at')
	pass
@admin.register(TypeService)
class TypeServiceAdmin(admin.ModelAdmin):
	list_display = ('name', 'create_at','modified_at')
	list_filter = ('name', 'create_at')
	pass
@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
	list_display = (
		'name',
		'comment',
		'create_at',
		'update_at',
		'area_id',
		'profitcenter_id',
		'type_service_id'
		)
	list_filter = ('name','comment','area_id','profitcenter_id','type_service_id','create_at','update_at')
	search_fields = ('name', 'area_id__name', 'profitcenter_id__name','type_service_id__name')
	pass
@admin.register(ClientService)
class ClientServiceAdmin(admin.ModelAdmin):
	list_display= (
		'amount',
		'pay_day',
		'type_sla',
		'type_contract',
		'client_id',
		'service_id',
		'start_date_c',
		'end_date_c'
		)
	list_filter = ('type_sla', 'type_contract','client_id__name','service_id__name')
	search_fields = ('type_sla', 'type_contract','client_id__name','service_id__name')
	pass
