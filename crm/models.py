from django.db import models
#defines
INDENTIFICATION_TYPES_CHOICES = {
	('R', 'RUT'),
	('D', 'DNI')
}
SLA_TYPES_CHOICES = {
	('A', '8x5'),
	('B', '24x7')
}
CONTRACT_TYPES_CHOICES = {
	('C', 'Definido'),
	('I', 'Indefinido')
}
class GroupClient(models.Model):
	name = models.CharField(max_length=144)
	create_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name
class Area(models.Model):
	name = models.CharField(max_length=144)
	create_at = models.DateTimeField(auto_now_add=True)
	update_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name
class ProfitCenter(models.Model):
	name = models.CharField(max_length=144)
	create_at = models.DateTimeField(auto_now_add=True)
	update_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name
class TypeService(models.Model):
	name = models.CharField(max_length=144)
	create_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name
class Client(models.Model):	
	name = models.CharField(max_length=144)
	business_name = models.CharField(max_length=144)
	activo = models.BooleanField(default=True)
	address_1 = models.CharField(max_length=144)
	identification_type = models.CharField(max_length=1, choices= INDENTIFICATION_TYPES_CHOICES)
	identification_number = models.CharField(max_length=50)	
	create_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)
	group_client = models.ForeignKey(GroupClient, on_delete=models.CASCADE)
	tecnical_contact = models.EmailField(blank=True)
	comercial_contact = models.EmailField(blank=True)
	finance_contact = models.EmailField(blank=True)

	def __str__(self):
		return self.name

class Service(models.Model):
	name = models.CharField(max_length=144)
	comment = models.BigIntegerField(default=0)
	create_at = models.DateTimeField(auto_now_add=True)
	update_at = models.DateTimeField(auto_now=True)
	area_id = models.ForeignKey(Area, on_delete=models.CASCADE)
	profitcenter_id = models.ForeignKey(ProfitCenter, on_delete=models.CASCADE)
	type_service_id = models.ForeignKey(TypeService, on_delete=models.CASCADE)

	def __str__(self):
		return self.name

class ClientService(models.Model):
	amount = models.DecimalField(max_digits=5, decimal_places=2)
	pay_day = models.IntegerField(default=0)
	service_id = models.ForeignKey(Service, on_delete=models.CASCADE)
	client_id = models.ForeignKey(Client, on_delete=models.CASCADE)
	profitcenter_id = models.ForeignKey(ProfitCenter, on_delete=models.CASCADE)
	type_sla = models.CharField(max_length=1, choices= SLA_TYPES_CHOICES)
	type_contract = models.CharField(max_length=1, choices= CONTRACT_TYPES_CHOICES)
	start_date_c = models.DateTimeField(auto_now_add=False)
	end_date_c = models.DateTimeField(auto_now_add=False,blank=True)

	def __str__(self):
		return 'Ok'





